import Vue from 'vue'
import Vuex, { Store } from 'vuex'

Vue.use(Vuex)

export default new Store({
  state: {
    dark: false
  },
  mutations: {
    toggleDark(state) {
      state.dark = !state.dark
      localStorage.setItem("darktheme", state.dark.toString());
    }
  },
  actions: {
    toggleDark(store) {
      store.commit('toggleDark')
    }
  },
  modules: {
  }
})
