import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueSimpleMarkdown from 'vue-simple-markdown'
//import 'vue-simple-markdown/dist/vue-simple-markdown.css'

Vue.config.productionTip = false
Vue.use(VueSimpleMarkdown)
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
